import {isogram} from './isosgram';

describe('isogram', () => {
    test('it returns false for "isograms"', () => {
        expect(isogram('isograms')).toBe(false)
    });

    test('it returns false for "aA"', () => {
        expect(isogram('aA')).toBe(false)
    });

    test('it returns true for "background"', () => {
        expect(isogram('background')).toBe(true)
    });

    test('it returns true for "downstream"', () => {
        expect(isogram('downstream')).toBe(true)
    });

    test('it returns true for "six-year-old"', () => {
        expect(isogram('six-year-old')).toBe(true)
    });

    test('it throw error for no paramter', () => {
        expect(function (){isogram()}).toThrow(new Error("There is no string"))
    });

});
