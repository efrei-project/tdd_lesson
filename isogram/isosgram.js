export function isogram(str) {

    if(!str) {
        throw new Error("There is no string");
    }

    if(str.length == 0) {
        throw new Error("There is an empty string");
    }

    let scannedCharacters = {};

    for (let i = 0; i < str.length; i++) {
        let currentCharacter = str[i].toLowerCase();

        // If character is not alphanumeric, continue next ite
        let reg = /[^a-z\d]/i;
        if (reg.test(currentCharacter)) continue;

        if (scannedCharacters[currentCharacter] === true) return false;

        scannedCharacters[currentCharacter] = true;
    }

    return true;
}
