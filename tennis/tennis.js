class Player{
    constructor(name){
        this.name = name;
        this.score = 0
    }
}

export class TennisGame{

    constructor(playerOne, playerTwo){
        this.playerOne = new Player(playerOne);
        this.playerTwo = new Player(playerTwo);
    }

    scoreAdd(player){
        switch (player.score) {
            case 0:
                player.score = 15;
                break;
            case 15:
                player.score = 30;
                break;
            case 30:
                player.score = 40;
                break;
            case 40:
                player.score = 99;
                break;
            case 99:
                player.score = 100;
                break;
        }
    }

    scorePlayerOne(){
        if(this.playerTwo.score === 99  && this.playerOne.score === 40){
            this.playerTwo.score = 40;
        } else  {
            this.scoreAdd(this.playerOne)
        }
    }

    scorePlayerTwo(){
        if(this.playerOne.score === 99  && this.playerTwo.score === 40){
            this.playerOne.score = 40;
        } else  {
            this.scoreAdd(this.playerTwo)
        }
    }

    displayScore(){
        let toReturn = '';
        if (this.playerOne.score === 40 && this.playerTwo.score === 40){
            toReturn = 'deuce'
        }
        else if (this.playerOne.score === 99 || this.playerTwo.score === 99){
            toReturn = `Avantage! ${this.playerOne.score === 99 ? this.playerOne.name : this.playerTwo.name}`
        }
        else if (this.playerOne.score === 100 || this.playerTwo.score === 100){
            toReturn = `Nous avons un gagnant: ${this.playerOne.score === 99 ? this.playerOne.name : this.playerTwo.name}`
        }
        else {
            toReturn = `${this.playerOne.score} - ${this.playerTwo.score}`
        }

        return toReturn
    }

}