import { TennisGame } from './tennis';

describe('tennis', () => {

    test('it return "0 - 0" for new game',  () =>{
        let game = (new TennisGame("Dimitri", "Pierre"));
        expect(game.displayScore()).toEqual("0 - 0")
    });

    test('it return "15 - 0"',  () =>{
        let game = (new TennisGame("Dimitri", "Pierre"));
        game.scorePlayerOne();
        expect(game.displayScore()).toEqual("15 - 0")
    });

    test('it return "40 - 0"',  () =>{
        let game = (new TennisGame("Dimitri", "Pierre"));
        game.scorePlayerOne();
        game.scorePlayerOne();
        game.scorePlayerOne();
        expect(game.displayScore()).toEqual("40 - 0")
    });

    test('it return "deuce"',  () =>{
        let game = (new TennisGame("Dimitri", "Pierre"));
        game.scorePlayerOne();
        game.scorePlayerOne();
        game.scorePlayerOne();
        game.scorePlayerTwo();
        game.scorePlayerTwo();
        game.scorePlayerTwo();
        expect(game.displayScore()).toEqual("deuce")
    });

    test('it return "Avantage! Pierre"',  () =>{
        let game = (new TennisGame("Dimitri", "Pierre"));
        game.scorePlayerOne();
        game.scorePlayerOne();
        game.scorePlayerOne();
        game.scorePlayerTwo();
        game.scorePlayerTwo();
        game.scorePlayerTwo();
        game.scorePlayerTwo();
        expect(game.displayScore()).toEqual("Avantage! Pierre")
    });

    test('it return "deuce"',  () =>{
        let game = (new TennisGame("Dimitri", "Pierre"));
        game.scorePlayerOne();
        game.scorePlayerOne();
        game.scorePlayerOne();
        game.scorePlayerTwo();
        game.scorePlayerTwo();
        game.scorePlayerTwo();
        game.scorePlayerTwo();
        game.scorePlayerOne();
        expect(game.displayScore()).toEqual("deuce")
    });

    test('it return "Nous avons un gagnant: Pierre"',  () =>{
        let game = (new TennisGame("Dimitri", "Pierre"));
        game.scorePlayerOne();
        game.scorePlayerOne();
        game.scorePlayerOne();
        game.scorePlayerTwo();
        game.scorePlayerTwo();
        game.scorePlayerTwo();
        game.scorePlayerTwo();
        game.scorePlayerTwo();
        expect(game.displayScore()).toEqual("Nous avons un gagnant: Pierre")
    });

});