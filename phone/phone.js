export class phoneNumberSanitizer {
    constructor(){};

    static sanitize(number){
        let properNum = "";

        if (!number) return null;
        if (!(typeof number === "string")) return null;
        if (number.length < 10) return null;

        if(/\.|-/.test(number)){
            properNum = number.replace(/\.|-/g, '')
        }

        if(/(\+|00)33/.test(number)){
            if(number.length === 12){
                properNum = number.replace(/\+33/, 0);
            } else if(number.length === 13){
                properNum = "0" + number.slice(4, number.length)
            }
        }

        return properNum === "" ? number : properNum;
    }
}