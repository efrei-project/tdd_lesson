import { phoneNumberSanitizer } from './phone';

describe('phone', () => {

    test('it return "0601020304" for "06.01.02.03.04"',  () =>{
        expect(phoneNumberSanitizer.sanitize("06.01.02.03.04")).toEqual("0601020304");
    });

    test('it return "0601020304" for "06-01-02-03-04"',  () =>{
        expect(phoneNumberSanitizer.sanitize("06-01-02-03-04")).toEqual("0601020304");
    });

    test('it return "0601020304" for "+33601020304"',  () =>{
        expect(phoneNumberSanitizer.sanitize("+33601020304")).toEqual("0601020304");
    });

    test('it return "0601020304" for "0033601020304"',  () =>{
        expect(phoneNumberSanitizer.sanitize("0033601020304")).toEqual("0601020304");
    });

    test('it return "0601020304" for "0601020304"',  () =>{
        expect(phoneNumberSanitizer.sanitize("0601020304")).toEqual("0601020304");
    });

    test('it return null for "hello"',  () =>{
        expect(phoneNumberSanitizer.sanitize("hello")).toEqual(null);
    });

    test('it throw error for "56321"',  () =>{
        expect(phoneNumberSanitizer.sanitize("56321")).toEqual(null);
    });

});